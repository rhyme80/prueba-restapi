package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
)

type Publication struct {
	ID    string `json:"id"`
	Value string `json:"value"`
	Url   string `json:"url"`
}

func main() {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/publications", getPublications)
	router.HandleFunc("/publicationsAsync", getPublicationsAsync)

	log.Fatal(http.ListenAndServe(":3000", router))
}

func consult(url string, publication Publication) Publication {
	response, err := http.Get(url)
	if err != nil {
		fmt.Println(err.Error())
	}
	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}
	json.Unmarshal(responseData, &publication)
	return publication
}

func getPublicationsAsync(writer http.ResponseWriter, request *http.Request) {
	var publications []Publication
	go func() {
		for i := 0; i < 25; i++ {
			var publication Publication
			publication = consult("https://api.chucknorris.io/jokes/random", publication)

			fmt.Println("publicacion: ", publication)
			publications = append(publications, publication)
		}
	}()
	defer json.NewEncoder(writer).Encode(publications)
}

func getPublications(writer http.ResponseWriter, request *http.Request) {
	var publications []Publication
	for i := 0; i < 25; i++ {
		var publication Publication
		publication = consult("https://api.chucknorris.io/jokes/random", publication)
		fmt.Println(publication)
		publications = append(publications, publication)
	}
	json.NewEncoder(writer).Encode(publications)

}

# prueba-restapi
### Author: Arthur Davis 

## Guia de ejecucion
* Clonar repositorio 
```
git clone https://gitlab.com/rhyme80/prueba-restapi.git
```
* Descargar librerias
```
go mod tidy
```
* Construir la imagen de docker
```
docker build -t pruebarestapi .
```
* Ejecutar contenedor
```
docker run -d --name pruebaapirun -p 3000:3000 pruebarestapi
```

## Endpoints
### Sincrono
http://localhost:3000/publications
### Asincrono
http://localhost:3000/publicationsAsync

"ver logs, en la consulta asincrona no se muestra los datos, solo en la consola, por ello ver los logs del contenedor"
```
docker logs pruebaapirun
```
